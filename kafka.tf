data "http" "myip" {
  url = "http://ipv4.icanhazip.com"
}


# security group for bastion host
resource "aws_security_group" "bastion_sg" {
  name        = "bastion_sg"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description      = "TLS from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["${chomp(data.http.myip.body)}/32"]

  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]

  }
}

# security group for cluster
resource "aws_security_group" "cluster_sg" {
  name        = "cluster_sg"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description      = "TLS from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["${chomp(aws_instance.bastion_host.private_ip)}/32"]

  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]

  }
}

# Instance for bastion host

resource "aws_instance" "bastion_host" {

ami = var.ami
subnet_id = element(aws_subnet.public_subnet.*.id, 1)
instance_type = var.instance_type
key_name = var.instance_key
associate_public_ip_address = true
vpc_security_group_ids = [aws_security_group.bastion_sg.id]

tags = {

Name = "bastion-host"

}
}
# Instance for cluster

resource "aws_instance" "cluster" {
count = length(var.cluster_no)
ami = var.ami
subnet_id = element(aws_subnet.private_subnet.*.id, count.index)
instance_type = var.instance_type
associate_public_ip_address = false
key_name = var.instance_key
vpc_security_group_ids = [aws_security_group.cluster_sg.id]
availability_zone = element(var.azs, count.index)

tags = {
Name = "kafka-${count.index+1}"
}
}

