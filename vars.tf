
variable "region" {
   default = "us-west-1"
}

variable "block" {
   default = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
 type = list
 default = ["10.0.1.0/24","10.0.2.0/24"]
}

variable "private_subnet_cidr" {
 type = list
 default = ["10.0.3.0/24","10.0.4.0/24"]
}

variable "bastion" {

type = string 
default = "main-host"
}
variable "cluster_no" {

type = list
default = ["kafka-1","kafka-2","kafka-3"]
}

variable "ami" {

type = string
default = "ami-053ac55bdcfe96e85"
}

variable "instance_type" {

default = "t2.micro"
}

variable "instance_key" {

type =  string
default = "n.california"

}

variable "azs" {

type = list
default = ["us-west-1a","us-west-1b"]
}
